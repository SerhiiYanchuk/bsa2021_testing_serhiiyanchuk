﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }      
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return $"ID: {Id}, Author ID: {AuthorId}, Team ID: {TeamId}, Name: {Name}, Description: {Description}, Deadline: {Deadline}";
        }
    }
}
