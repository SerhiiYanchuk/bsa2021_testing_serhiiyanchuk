using BSA2021_ProjectStructure.Common.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace BSA2021_ProjectStructure.IntegrationTests
{
    public class UserControllerIntegrationTests: IClassFixture<CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup>>
    {
        private readonly HttpClient _client;
        public UserControllerIntegrationTests(CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup> factory)
        {
            _client = factory.CreateClient();
        }
        private async System.Threading.Tasks.Task<int> PostEntity(dynamic newEntity, string route)
        {
            string jsonInString = JsonConvert.SerializeObject(newEntity);

            var httpResponse = await _client.PostAsync($"api/{route}", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResponce = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode == HttpStatusCode.Created)
            {
                return JsonConvert.DeserializeObject<dynamic>(stringResponce).id;
            }
            else
            {
                throw new InvalidOperationException($"Post request on route api/{route} has not responce 201. Status code: {(int)httpResponse.StatusCode}");
            }
        }
        [Fact]
        public async System.Threading.Tasks.Task DeleteUser_WhenExistingUserId_ThenResponseCode204AndGetByIdResponseCode404()
        {
            // arrange
            int generatedId = await PostEntity(new NewUserDTO(), "users");

            // act/assert
            var httpResponse = await _client.DeleteAsync($"api/users/{generatedId}");
            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/users/{generatedId}");
            Assert.Equal(HttpStatusCode.NotFound, httpResponseGetById.StatusCode);
        }
        [Fact]
        public async System.Threading.Tasks.Task DeleteUser_WhenNotExistingUserId_ThenResponseCode404()
        {
            // act
            var httpResponse = await _client.DeleteAsync($"api/users/1");
            // assert
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
        [Fact]
        public async System.Threading.Tasks.Task GetUserProjectsWithQuantity_WhenExistingUserWith2TasksIn1Project_ThenResponseCode200AndGetDictionaryKeyValueProjectAnd2()
        {
            // arrange
            int userId = await PostEntity(new NewUserDTO(), "users");
            int teamId = await PostEntity(new NewTeamDTO(), "teams");
            int projectId = await PostEntity(new NewProjectDTO { AuthorId = userId, TeamId = teamId }, "projects");
            int taskId1 = await PostEntity(new NewTaskDTO { ProjectId = projectId, PerformerId = userId }, "tasks");
            int taskId2 = await PostEntity(new NewTaskDTO { ProjectId = projectId, PerformerId = userId }, "tasks");

            // act/assert
            var httpResponse = await _client.GetAsync($"api/users/{userId}/projects");
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            string stringResponce = await httpResponse.Content.ReadAsStringAsync();
            IDictionary<ProjectDTO, int> dict = JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<ProjectDTO, int>>>(stringResponce).ToDictionary(p => p.Key, p => p.Value);
          
            var key = dict.Keys.Where(p => p.Id == projectId).FirstOrDefault();
            Assert.Equal(2, dict[key]);

            await _client.DeleteAsync($"api/users/{userId}");
            await _client.DeleteAsync($"api/teams/{teamId}");
            await _client.DeleteAsync($"api/projects/{projectId}");
            await _client.DeleteAsync($"api/tasks/{taskId1}");
            await _client.DeleteAsync($"api/tasks/{taskId2}");
        }
        [Fact]
        public async System.Threading.Tasks.Task GetUserProjectsWithQuantity_WhenNotExistingUserId_ThenResponseCode404()
        {
            // act
            var httpResponse = await _client.GetAsync($"api/users/1/projects");
            // assert
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenExistingUserWith2UnfinishedTasks_ThenResponseCode200AndGetListWith2UserUnfinishedTasks()
        {
            // arrange
            int userId = await PostEntity(new NewUserDTO(), "users");          
            int taskId1 = await PostEntity(new NewTaskDTO { PerformerId = userId }, "tasks");
            int taskId2 = await PostEntity(new NewTaskDTO { PerformerId = userId }, "tasks");
            int taskId3 = await PostEntity(new NewTaskDTO { PerformerId = userId + 1 }, "tasks");

            // act/assert
            var httpResponse = await _client.GetAsync($"api/users/{userId}/tasks/unfinished");
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

            string stringResponce = await httpResponse.Content.ReadAsStringAsync();
            IEnumerable<TaskDTO> list = JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(stringResponce);

            Assert.Equal(2, list.Count());

            await _client.DeleteAsync($"api/users/{userId}");
            await _client.DeleteAsync($"api/tasks/{taskId1}");
            await _client.DeleteAsync($"api/tasks/{taskId2}");
            await _client.DeleteAsync($"api/tasks/{taskId3}");
        }
        [Fact]
        public async System.Threading.Tasks.Task GetUserUnfinishedTasks_WhenNotExistingUserId_ThenResponseCode404()
        {
            // act
            var httpResponse = await _client.GetAsync("api/users/1/tasks/unfinished");
            // assert
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}
