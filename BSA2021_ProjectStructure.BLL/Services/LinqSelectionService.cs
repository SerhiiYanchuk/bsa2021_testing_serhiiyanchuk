﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using BSA2021_ProjectStructure.Common.EqualityComparer;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class LinqSelectionService : ILinqSelectionService
    {
        private readonly IEnumerable<ProjectDTO> _projects;
        public LinqSelectionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var projects = mapper.Map<IEnumerable<ProjectDTO>>(unitOfWork.Projects.GetAll(false));
            var tasks = mapper.Map<IEnumerable<TaskDTO>>(unitOfWork.Tasks.GetAll(false));
            var teams = mapper.Map<IEnumerable<TeamDTO>>(unitOfWork.Teams.GetAll(false));
            var users = mapper.Map<IEnumerable<UserDTO>>(unitOfWork.Users.GetAll(false));
            _projects = CreateStructure(projects, tasks, teams, users);
        }
        public IDictionary<ProjectDTO, int> GetUserProjectsWithQuantityTask(int performerId)
        {
            return _projects.SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                            .Where(p => p.Task.PerformerId == performerId)
                            .GroupBy(p => p.Project, new ProjectEqualityComparer())
                            .ToDictionary(g => g.Key, g => g.Count());
        }

        public IEnumerable<TaskDTO> GetUserTasks(int performerId)
        {
            const int MaxNameLength = 45;
            return _projects.SelectMany(p => p.Tasks)
                           .Where(t => t.PerformerId == performerId && t.Name.Length < MaxNameLength)
                           .ToList();                        
        }

        public IEnumerable<FinishedTaskDTO> GetFinishedTaskInCurrentYear(int performerId)
        {
            const int CurrentYear = 2021;
            return _projects.SelectMany(p => p.Tasks)
                            .Where(t => t.PerformerId == performerId)
                            .Where(t => t.FinishedAt.HasValue && t.FinishedAt.Value.Year == CurrentYear)
                            .Select(t => new FinishedTaskDTO
                            {
                                Id = t.Id,
                                Name = t.Name
                            })
                            .ToList();
        }

        public IEnumerable<TeamShortInfoDTO> GetTeamShortInfo()
        {
            const int MinAge = 10;
            return _projects.Select(p => p.Team)
                           .Distinct(new TeamEqualityComparer())
                           .Where(t => t.Members.All(m => DateTime.Now.Year - m.Birthday.Year >= MinAge))
                           .Select(t => new TeamShortInfoDTO
                           {
                               Id = t.Id,
                               Name = t.Name,
                               Members = t.Members.OrderByDescending(m => m.RegisteredAt).ToList()
                           })
                          .ToList();
        }

        public IEnumerable<PerformerWithTasksDTO> GetUsersWithTasks()
        {
            return _projects.SelectMany(p => p.Tasks)
                           .GroupBy(t => t.Performer, new UserEqualityComparer())
                           .Select(g => new PerformerWithTasksDTO
                           {
                               Id = g.Key.Id,
                               TeamId = g.Key.TeamId,
                               FirstName = g.Key.FirstName,
                               LastName = g.Key.LastName,
                               Email = g.Key.Email,
                               RegisteredAt = g.Key.RegisteredAt,
                               Birthday = g.Key.Birthday,
                               Tasks = g.OrderByDescending(t => t.Name.Length).ToList()
                           })
                           .OrderBy(performer => performer.FirstName)
                           .ToList();
        }

        public UserDetailDTO GetUserDetail(int userId)
        {
            return _projects.Where(p => p.AuthorId == userId)
                           .GroupBy(p => p.Author, new UserEqualityComparer())
                           .Select(g => new
                           {
                               User = g.Key,
                               LastProject = g.OrderByDescending(p => p.CreatedAt).First(),
                               UserTasks = _projects.SelectMany(p => p.Tasks).Where(t => t.PerformerId == g.Key.Id)
                           })
                           .Select(g => new UserDetailDTO
                           {
                               User = g.User,
                               LastProject = g.LastProject,
                               ProjectTasksCount = g.LastProject.Tasks.Count(),
                               UserUnfinishedTasksCount = g.UserTasks.Count(t => !t.FinishedAt.HasValue),
                               LongestTask = g.UserTasks.OrderByDescending(t => t.FinishedAt.HasValue ? (t.FinishedAt - t.CreatedAt) : (DateTime.Now - t.CreatedAt)).FirstOrDefault()
                           })
                           .FirstOrDefault();
        }

        public IEnumerable<ProjectDetailDTO> GetProjectsDetail()
        {
            const int MinDescriptionLength = 20;
            const int MaxProjectTasks = 3;
            return _projects.Select(p => new ProjectDetailDTO
                            {
                                Project = p,
                                LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                                ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                TeamMembersCount = p.Description.Length > MinDescriptionLength || p.Tasks.Count() < MaxProjectTasks ? p.Team.Members.Count() : new int?()
                            })
                           .ToList();
        }

        private IEnumerable<ProjectDTO> CreateStructure(IEnumerable<ProjectDTO> projects, IEnumerable<TaskDTO> tasks, IEnumerable<TeamDTO> teams, IEnumerable<UserDTO> users)
        {
            var tasksWithPerformers = from task in tasks
                                      join performer in users on task.PerformerId equals performer.Id
                                      select new TaskDTO
                                      {
                                          Id = task.Id,
                                          ProjectId = task.ProjectId,
                                          PerformerId = task.PerformerId,
                                          Performer = performer,
                                          Name = task.Name,
                                          Description = task.Description,
                                          State = task.State,
                                          CreatedAt = task.CreatedAt,
                                          FinishedAt = task.FinishedAt,
                                      };

            return (from project in projects
                    join task in tasksWithPerformers on project.Id equals task.ProjectId into taskList
                    join author in users on project.AuthorId equals author.Id
                    join team in teams on project.TeamId equals team.Id
                    join member in users on team.Id equals member.TeamId into memberList
                    select new ProjectDTO
                    {
                        Id = project.Id,
                        AuthorId = project.AuthorId,
                        Author = author,
                        TeamId = project.TeamId,
                        Team = new TeamDTO
                        {
                            Id = team.Id,
                            Name = team.Name,
                            CreatedAt = team.CreatedAt,
                            Members = memberList.ToList()
                        },
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        Tasks = taskList.ToList()
                    }).ToList();
        }
    }
}
