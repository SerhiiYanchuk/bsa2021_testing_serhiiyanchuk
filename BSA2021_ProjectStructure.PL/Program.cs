﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using BSA2021_ProjectStructure.PL.Models.QueryResultModels;
using BSA2021_ProjectStructure.PL.Services;
using BSA2021_ProjectStructure.PL.Services.Factories;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace BSA2021_ProjectStructure.PL
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration config = GetConfig();
            string serverUrl = config["ServerUrl"];
            while (true)
            {
                Console.Clear();
                Console.WriteLine("-----------------------");
                Console.WriteLine($"{' ',-3}Project structure\n{' ',-3}Main menu");
                Console.WriteLine("-----------------------\n");
                Console.WriteLine("1 - LINQ query");
                Console.WriteLine("2 - CRUD projects");
                Console.WriteLine("3 - CRUD tasks");
                Console.WriteLine("4 - CRUD teams");
                Console.WriteLine("5 - CRUD users");

                Console.WriteLine("0 - Exit");
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            using (IHttpRequestToQueryService service = new HttpRequestToQueryService(serverUrl))
                            {
                                SubMenuWithLinqQuery(service);
                            }                               
                            break;
                        case 2:
                            SubMenuCRUD(new CrudService<ProjectModel, NewProjectModel>(serverUrl + "/api/projects", new ProjectFactory()), "project");
                            break;
                        case 3:
                            SubMenuCRUD(new CrudService<TaskModel, NewTaskModel>(serverUrl + "/api/tasks", new TaskFactory()), "tasks");
                            break;
                        case 4:
                            SubMenuCRUD(new CrudService<TeamModel, NewTeamModel>(serverUrl + "/api/teams", new TeamFactory()), "team");
                            break;
                        case 5:
                            SubMenuCRUD(new CrudService<UserModel, NewUserModel>(serverUrl + "/api/users", new UserFactory()), "user");
                            break;
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                }
            }


        }
        static IConfiguration GetConfig()
        {
            FileInfo file = new FileInfo($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\config.json");
            if (!file.Exists)
                throw new Exception("Config file doesn't exist");
            var builder = new ConfigurationBuilder()
                    .AddJsonFile(file.FullName, optional: false);
            return builder.Build();
        }
        static void SubMenuWithLinqQuery(IHttpRequestToQueryService service)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("----------------------");
                Console.WriteLine($"{' ',-3}Project structure\n{' ',-3}LINQ query");

                Console.WriteLine("----------------------");
                Console.WriteLine("1 - Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).");
                Console.WriteLine("2 - Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).");
                Console.WriteLine("3 - Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id).");
                Console.WriteLine("4 - Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.");
                Console.WriteLine("5 - Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию)");
                Console.WriteLine(@"6 - Получить следующую структуру (передать Id пользователя в параметры):
                                        User
                                        Последний проект пользователя(по дате создания)
                                        Общее кол - во тасков под последним проектом
                                        Общее кол - во незавершенных или отмененных тасков для пользователя
                                        Самый долгий таск пользователя по дате(раньше всего создан - позже всего закончен)");
                Console.WriteLine(@"7 - Получить следующую структуру:
                                        Проект
                                        Самый длинный таск проекта (по описанию)
                                        Самый короткий таск проекта (по имени)
                                        Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3");
                Console.WriteLine("0 - Back");
                Console.Write("> ");

                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            Console.Write("Id пользователя > ");
                            IDictionary<ProjectModel, int> selection1 = service.GetUserProjectsWithQuantityTask(int.Parse(Console.ReadLine()));

                            Console.WriteLine();
                            foreach (var item in selection1)
                                Console.WriteLine($"{"",5}Проект: {item.Key.Name,-35} К-ство тасков: {item.Value}");
                            break;
                        case 2:
                            Console.Write("Id пользователя > ");
                            IEnumerable<TaskModel> selection2 = service.GetUserTasks(int.Parse(Console.ReadLine()));

                            Console.WriteLine();
                            foreach (var item in selection2)
                                Console.WriteLine($"{"",5}Таск: {item.Name,-52} Длина названия: {item.Name.Length}");
                            break;
                        case 3:
                            Console.Write("Id пользователя > ");
                            IEnumerable<FinishedTaskModel> selection3 = service.GetFinishedTaskInCurrentYear(int.Parse(Console.ReadLine()));

                            Console.WriteLine();
                            foreach (var item in selection3)
                                Console.WriteLine($"{"",5}Id: {item.Id,-5} Название таска: {item.Name}");
                            break;
                        case 4:
                            IEnumerable<TeamShortInfoModel> selection4 = service.GetTeamShortInfo();

                            Console.WriteLine();
                            foreach (var item in selection4)
                            {
                                Console.WriteLine($"{"",5}Id: {item.Id,-5} Команда: {item.Name}");
                                foreach (var item2 in item.Members)
                                    Console.WriteLine($"{"",10}Фамилия: {item2.LastName,-20} Год рождения: {item2.Birthday.Year,-10}  Дата регистрации: {item2.RegisteredAt,-20}");
                                Console.WriteLine();
                            }

                            break;
                        case 5:
                            IEnumerable<PerformerWithTasksModel> selection5 = service.GetUsersWihtTasks();

                            Console.WriteLine();
                            foreach (var item in selection5)
                            {
                                Console.WriteLine($"{"",5}Имя исполнителя: {item.FirstName}");
                                foreach (var item2 in item.Tasks)
                                    Console.WriteLine($"{"",10}Название таска: {item2.Name}");
                                Console.WriteLine();
                            }
                            break;
                        case 6:
                            Console.Write("Id пользователя > ");
                            UserDetailModel selection6 = service.GetUserDetail(int.Parse(Console.ReadLine()));

                            if (selection6 is not null)
                            {
                                Console.WriteLine($"\n{"",5}Пользователь: {selection6.User.FirstName + " " + selection6.User.LastName}");
                                Console.WriteLine($"{"",10}Дата последнего проекта: {selection6.LastProject.CreatedAt}");
                                Console.WriteLine($"{"",10}К-ство таксов последнего проекта: {selection6.ProjectTasksCount}");
                                Console.WriteLine($"{"",10}К-ство незвершенных тасков пользователя: {selection6.UserUnfinishedTasksCount}");
                                string end = selection6.LongestTask.FinishedAt.HasValue ? selection6.LongestTask.FinishedAt.ToString() : "до этих пор";
                                Console.WriteLine($"{"",10}Самый долгий таск пользователя: {selection6.LongestTask.CreatedAt} - {end}");
                            }

                            break;
                        case 7:
                            IEnumerable<ProjectDetailModel> selection7 = service.GetProjectsDetail();

                            foreach (var item in selection7)
                            {
                                Console.WriteLine($"\n{"",5}Проект: {item.Project.Name}");
                                Console.WriteLine($"{"",10}Описание самого длинного таска по описанию: {item.LongestDescriptionTask?.Description ?? "Тасков нет",-50}");
                                Console.WriteLine($"{"",10}Имя самого короткого такса по имени: {item.ShortestNameTask?.Name ?? "Тасков нет",-50}");
                                string count = item.TeamMembersCount.HasValue ? item.TeamMembersCount.Value.ToString() : "не выполнилось условие \"описание проекта > 20 символов или кол-во тасков < 3\"";
                                Console.WriteLine($"{"",10}Общее кол-во пользователей в команде проекта: {count,-50}");
                            }
                            break;
                        case 0:
                            return;
                        default:
                            Console.Write("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                    Console.WriteLine("\nPress any key");
                    Console.ReadKey();
                }
            }
        }
        static void SubMenuCRUD<TEntity, TNewEntity>(ICrudService<TEntity, TNewEntity> service, string entityName)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("----------------------");
                Console.WriteLine($"{' ',-3}Project structure\n{' ',-3}CRUD {entityName}");

                Console.WriteLine("----------------------");
                Console.WriteLine("1 - Получить все сущности");
                Console.WriteLine("2 - Получить сущность по ID");
                Console.WriteLine("3 - Добавить новую сущность");
                Console.WriteLine("4 - Редактировать сущность");
                Console.WriteLine("5 - Удалить сущность по ID");

                Console.WriteLine("0 - Back");
                Console.Write("> ");

                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            IEnumerable<TEntity> selection = service.GetAll();
                            foreach (var entity in selection)
                                Console.WriteLine(entity);
                            break;
                        case 2:
                            Console.Write("ID > ");
                            TEntity entityById = service.FindById(int.Parse(Console.ReadLine()));
                            if (entityById is not null)
                                Console.WriteLine(entityById);
                            break;
                        case 3:
                            TNewEntity newEntity = service.ModelFactory.CreateModel();
                            TEntity createdEntity = service.Insert(newEntity);
                            Console.WriteLine("Created entity");
                            Console.WriteLine(createdEntity);
                            break;
                        case 4:
                            Console.Write("ID > ");
                            TEntity entityEdit = service.FindById(int.Parse(Console.ReadLine()));
                            if (entityEdit is null)
                            {
                                Console.WriteLine("ID doesn't exist");
                                break;
                            }                              
                            service.ModelFactory.UpdateModel(entityEdit);
                            service.Update(entityEdit);
                            break;
                        case 5:
                            Console.Write("ID > ");
                            service.Delete(int.Parse(Console.ReadLine()));
                            break;
                        case 0:
                            return;
                        default:
                            Console.Write("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                    Console.WriteLine("\nPress any key");
                    Console.ReadKey();
                }
            }
        }
    }
}
