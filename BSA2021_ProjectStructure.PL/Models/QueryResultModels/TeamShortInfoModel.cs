﻿using System.Collections.Generic;

namespace BSA2021_ProjectStructure.PL.Models.QueryResultModels
{
    public class TeamShortInfoModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserModel> Members { get; set; } = new List<UserModel>();
    }
}
