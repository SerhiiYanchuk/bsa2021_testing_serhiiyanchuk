﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Infrastructure;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.BLL.Services;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace BSA2021_ProjectStructure.Tests
{
    public class UserServiceTests
    {
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;
        public UserServiceTests()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            var mapper = new MapperConfiguration(cfg => {
                cfg.AddProfile<ProjectMapperProfile>();
            }).CreateMapper();
            _userService = new UserService(_unitOfWork, mapper);
        }
        [Fact]
        public void Insert_WhenInsertValidNewUser_ThenGetInsertedUserWithGeneretedId()
        {
            // arrange 
            A.CallTo(() => _unitOfWork.Users.Insert(A<User>.Ignored)).Invokes((User newUser) => { newUser.Id = 1; });
            NewUserDTO newUser = new NewUserDTO();
            // act
            UserDTO insertedUser = _userService.Insert(newUser);
            // assert    
            A.CallTo(() => _unitOfWork.SaveChanges()).MustHaveHappened();
            Assert.Equal(1, insertedUser.Id);
        }
        [Fact]
        public void Update_WhenAddTeamIdInExistingUserAndUpdate_ThenUpdateHappened()
        {
            // arrange 
            UserDTO editUser = new UserDTO { Id = 1, TeamId = 1};
            // act
            _userService.Update(editUser);
            // assert    
            A.CallTo(() => _unitOfWork.SaveChanges()).MustHaveHappened();
            A.CallTo(() => _unitOfWork.Users.Update(A<User>.Ignored)).MustHaveHappened();
        }
        [Fact]
        public void FindUserUnfinishedTasks_WhenExistingUserWith2UnfinishedTasks_ThenGetListWith2UserUnfinishedTask()
        {
            // arrange 
            A.CallTo(() => _unitOfWork.Tasks.Find(A<Expression<Func<Task, bool>>>.Ignored, A<bool>.Ignored))
                .Returns(new List<Task> { new Task { PerformerId = 1, FinishedAt = null},
                                          new Task { PerformerId = 1, FinishedAt = null} });
            // act
            var tasks = _userService.FindUserUnfinishedTasks(1);
            // assert    
            Assert.True(tasks.All(t => t.PerformerId == 1 && t.FinishedAt == null));
            Assert.Equal(2, tasks.Count());
        }
        [Fact]
        public void FindUserUnfinishedTasks_WhenExistingUserWithoutUnfinishedTasks_ThenGetEmptyList()
        {
            // arrange
            A.CallTo(() => _unitOfWork.Tasks.Find(A<Expression<Func<Task, bool>>>.Ignored, A<bool>.Ignored))
                .Returns(new List<Task>());
            // act
            var tasks = _userService.FindUserUnfinishedTasks(1);
            // assert    
            Assert.Empty(tasks);
        }
    }
}
