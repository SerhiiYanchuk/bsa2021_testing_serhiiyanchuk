﻿using BSA2021_ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface ITaskRepository : IRepository<Task>
    {
        // you can add specific functionality for Task
        void LoadProject(Task task);
        void LoadPerformer(Task task);
    }
}
