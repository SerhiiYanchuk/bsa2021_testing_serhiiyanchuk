﻿

namespace BSA2021_ProjectStructure.Common.DTO.QueryResultDTO
{
    public class ProjectDetailDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestDescriptionTask { get; set; }
        public TaskDTO ShortestNameTask { get; set; }
        public int? TeamMembersCount { get; set; }
    }
}
