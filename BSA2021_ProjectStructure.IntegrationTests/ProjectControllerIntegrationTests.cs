using BSA2021_ProjectStructure.Common.DTO;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace BSA2021_ProjectStructure.IntegrationTests
{
    public class ProjectControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup>>
    {
        private readonly HttpClient _client;
        public ProjectControllerIntegrationTests(CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup> factory)
        {
            _client = factory.CreateClient();
        }
        [Fact]
        public async System.Threading.Tasks.Task PostProject_WhenValidBody_ThenResponseCode201AndGetByIdResponseCode200()
        {
            // arrange
            string jsonInString = JsonConvert.SerializeObject(new NewProjectDTO());

            // act/assert
            var httpResponse = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResponce = await httpResponse.Content.ReadAsStringAsync();
            int generatedId = JsonConvert.DeserializeObject<ProjectDTO>(stringResponce).Id;

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/projects/{generatedId}");
            Assert.Equal(HttpStatusCode.OK, httpResponseGetById.StatusCode);

            await _client.DeleteAsync($"api/projects/{generatedId}");
        }
        [Theory]
        [InlineData("some data")]
        public async System.Threading.Tasks.Task PostProject_WhenNotValidBody_ThenResponseCode400(string jsonInString)
        {
            // act
            var httpResponse = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            // assert
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
