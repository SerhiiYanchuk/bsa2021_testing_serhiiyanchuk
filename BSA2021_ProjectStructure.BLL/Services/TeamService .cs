﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<TeamDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_unitOfWork.Teams.GetAll(false));
        }
        public TeamDTO FindById(int teamId)
        {
            return _mapper.Map<TeamDTO>(_unitOfWork.Teams.FindById(teamId));
        }
        public TeamDTO Insert(NewTeamDTO teamDTO)
        {
            var createdTeam = _mapper.Map<Team>(teamDTO);
            _unitOfWork.Teams.Insert(createdTeam);
            _unitOfWork.SaveChanges();
            return _mapper.Map<TeamDTO>(createdTeam);
        }

        public void Update(TeamDTO teamDTO)
        {
            Team editTeam = _unitOfWork.Teams.FindById(teamDTO.Id);
            _mapper.Map<TeamDTO, Team>(teamDTO, editTeam);
            _unitOfWork.Teams.Update(editTeam);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int teamId)
        {
            _unitOfWork.Teams.Delete(teamId);
            _unitOfWork.SaveChanges();
        }

        public void Delete(TeamDTO teamDTO)
        {
            _unitOfWork.Teams.Delete(_mapper.Map<Team>(teamDTO));
            _unitOfWork.SaveChanges();
        }
        public bool CheckAvailability(int teamId)
        {
            return _unitOfWork.Teams.CheckAvailability(teamId);
        }
       
    }
}
