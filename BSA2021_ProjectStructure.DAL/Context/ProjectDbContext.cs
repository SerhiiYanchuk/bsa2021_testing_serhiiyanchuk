﻿using BSA2021_ProjectStructure.DAL.Context.Configuration;
using BSA2021_ProjectStructure.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Reflection;

namespace BSA2021_ProjectStructure.DAL.Context
{
    public class ProjectDbContext : DbContext
    {
        public DbSet<Project> Projects { get; private set; }
        public DbSet<User> Users { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SetRealtion();
            //modelBuilder.Seed();
   
            modelBuilder.ApplyConfiguration(new ProjectConfiguration());
            modelBuilder.ApplyConfiguration(new TeamConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new TaskConfiguration());
        }
    }

    public class ProjectDbContextFactory : IDesignTimeDbContextFactory<ProjectDbContext>
    {
        public ProjectDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>();

            FileInfo file = new FileInfo($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\config.json");
            if (!file.Exists)
                throw new Exception("Config file doesn't exist");
            IConfiguration config = new ConfigurationBuilder()
                                   .AddJsonFile(file.FullName, optional: false)
                                   .Build();

            string connectionString = config.GetConnectionString("ProjectDatabase");
            optionsBuilder.UseSqlServer(connectionString, opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));

            return new ProjectDbContext(optionsBuilder.Options);
        }
    }
}
