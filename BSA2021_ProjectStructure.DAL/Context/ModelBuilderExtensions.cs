﻿using BSA2021_ProjectStructure.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BSA2021_ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void SetRealtion(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().HasMany(p => p.Tasks)
                                          .WithOne(t => t.Project)
                                          .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Team>().HasMany(t => t.Members)
                                       .WithOne(m => m.Team)
                                       .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Team>().HasMany(t => t.Projects)
                                       .WithOne(p => p.Team)
                                       .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>().HasMany(u => u.Tasks)
                                       .WithOne(t => t.Performer)
                                       .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>().HasMany(u => u.Projects)
                                       .WithOne(p => p.Author)
                                       .OnDelete(DeleteBehavior.Cascade);

        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // This data always goes into memory at every start, the only solution to free memory is manually setting state state 'detached' every time
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Project
                var response = GetData(client, "Projects");
                var projects = JsonConvert.DeserializeObject<List<Project>>(response.Content.ReadAsStringAsync().Result);
                projects.ForEach(p => { p.Id++; p.AuthorId++; p.TeamId++; });

                modelBuilder.Entity<Project>().HasData(projects);
                int maxFreeId = projects.Max(p => p.Id) + 1;
                modelBuilder.Entity<Project>().Property(p => p.Id).UseIdentityColumn(maxFreeId, 1);

                // Task
                response = GetData(client, "Tasks");
                var tasks = JsonConvert.DeserializeObject<List<Task>>(response.Content.ReadAsStringAsync().Result);
                tasks.ForEach(p => { p.Id++; p.ProjectId++; p.PerformerId++; });

                modelBuilder.Entity<Task>().HasData(tasks);
                maxFreeId = tasks.Max(p => p.Id) + 1;
                modelBuilder.Entity<Task>().Property(p => p.Id).UseIdentityColumn(maxFreeId, 1);

                // Team
                response = GetData(client, "Teams");
                var teams = JsonConvert.DeserializeObject<List<Team>>(response.Content.ReadAsStringAsync().Result);
                teams.ForEach(p => { p.Id++; });

                modelBuilder.Entity<Team>().HasData(teams);
                maxFreeId = teams.Max(p => p.Id) + 1;
                modelBuilder.Entity<Team>().Property(p => p.Id).UseIdentityColumn(maxFreeId, 1);

                // User
                response = GetData(client, "Users");
                var users = JsonConvert.DeserializeObject<List<User>>(response.Content.ReadAsStringAsync().Result);
                users.ForEach(p => { p.Id++; p.TeamId++; });

                modelBuilder.Entity<User>().HasData(users);
                maxFreeId = users.Max(p => p.Id) + 1;
                modelBuilder.Entity<User>().Property(p => p.Id).UseIdentityColumn(maxFreeId, 1);
            }
        }
        private static HttpResponseMessage GetData(HttpClient httpClient, string route)
        {
            var response = httpClient.GetAsync($"https://bsa21.azurewebsites.net/api/{route}").Result;
            if (!response.IsSuccessStatusCode)
                throw new Exception($"Status: {response.StatusCode}, {response.Content}");
            return response;
        }
    }
}
