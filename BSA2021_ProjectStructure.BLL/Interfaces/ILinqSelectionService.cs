﻿using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface ILinqSelectionService
    {
        IDictionary<ProjectDTO, int> GetUserProjectsWithQuantityTask(int performerId);
        IEnumerable<TaskDTO> GetUserTasks(int performerId);
        IEnumerable<FinishedTaskDTO> GetFinishedTaskInCurrentYear(int performerId);
        IEnumerable<TeamShortInfoDTO> GetTeamShortInfo();
        IEnumerable<PerformerWithTasksDTO> GetUsersWithTasks();
        UserDetailDTO GetUserDetail(int userId);
        IEnumerable<ProjectDetailDTO> GetProjectsDetail();
    }
}
