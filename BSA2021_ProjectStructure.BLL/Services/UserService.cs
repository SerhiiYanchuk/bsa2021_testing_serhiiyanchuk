﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<UserDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_unitOfWork.Users.GetAll(false));
        }
        public UserDTO FindById(int userId)
        {
            return _mapper.Map<UserDTO>(_unitOfWork.Users.FindById(userId));
        }
        public UserDTO Insert(NewUserDTO userDTO)
        {
            var createdUser = _mapper.Map<User>(userDTO);
            _unitOfWork.Users.Insert(createdUser);
            _unitOfWork.SaveChanges();
            return _mapper.Map<UserDTO>(createdUser);
        }

        public void Update(UserDTO userDTO)
        {
            User editUser = _unitOfWork.Users.FindById(userDTO.Id);
            _mapper.Map<UserDTO, User>(userDTO, editUser);
            _unitOfWork.Users.Update(editUser);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int userId)
        {
            _unitOfWork.Users.Delete(userId);
            _unitOfWork.SaveChanges();
        }

        public void Delete(UserDTO userDTO)
        {
            _unitOfWork.Users.Delete(_mapper.Map<User>(userDTO));
            _unitOfWork.SaveChanges();
        }
        public bool CheckAvailability(int userId)
        {
            return _unitOfWork.Users.CheckAvailability(userId);
        }

        public IEnumerable<TaskDTO> FindUserUnfinishedTasks(int performerId)
        {
            IEnumerable<Task> tasks = _unitOfWork.Tasks.Find(t => t.PerformerId == performerId && t.FinishedAt == null);
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }
    }
}
