﻿using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        // you can add specific functionality for User
        void LoadProjects(User user);
        void LoadTasks(User user);
    }
}
