﻿using BSA2021_ProjectStructure.PL.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace BSA2021_ProjectStructure.PL.Services
{
    public class CrudService<TEntity, TNewEntity> : ICrudService<TEntity, TNewEntity> where TEntity : class
                                                                                      where TNewEntity : class
    {
        private readonly string _routeToEnitity;
        private readonly HttpClient _client = new HttpClient();

        public IModelFactory<TEntity, TNewEntity> ModelFactory { get; }

        public CrudService(string routeToEntity, IModelFactory<TEntity, TNewEntity> modelFactory)
        {
            _routeToEnitity = routeToEntity;
            ModelFactory = modelFactory;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public IEnumerable<TEntity> GetAll()
        {
            var response = GetData();
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<TEntity>>(response.Content.ReadAsStringAsync().Result);
            return null;           
        }
        public TEntity FindById(int id)
        {
            var response = GetData(id);
            if (response is not null)
                return JsonConvert.DeserializeObject<TEntity>(response.Content.ReadAsStringAsync().Result);
            return null;
        }
        public TEntity Insert(TNewEntity item)
        {
            var response = PostData(item);
            if (response is not null)
                return JsonConvert.DeserializeObject<TEntity>(response.Content.ReadAsStringAsync().Result);
            return null;
        }
        public void Update(TEntity item)
        {
            PutData(item);
        }
        public void Delete(int id)
        {
            DeleteData(id);
        }       
        
        private HttpResponseMessage GetData(int? id = null)
        {
            var response = _client.GetAsync(_routeToEnitity + "/" + id ?? "").Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        private HttpResponseMessage PostData(TNewEntity newEntity)
        {
            var response = _client.PostAsJsonAsync(_routeToEnitity, newEntity).Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        private HttpResponseMessage PutData(TEntity entity)
        {
            var response = _client.PutAsJsonAsync(_routeToEnitity, entity).Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        private HttpResponseMessage DeleteData(int id)
        {
            var response = _client.DeleteAsync(_routeToEnitity + "/" + id).Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
    }
}
