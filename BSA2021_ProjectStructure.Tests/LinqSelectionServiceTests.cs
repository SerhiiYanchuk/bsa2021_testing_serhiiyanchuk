﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Infrastructure;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.BLL.Services;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace BSA2021_ProjectStructure.Tests
{
    public class LinqSelectionServiceTests
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LinqSelectionServiceTests()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            _mapper = new MapperConfiguration(cfg => {
                cfg.AddProfile<ProjectMapperProfile>();
            }).CreateMapper();

        }
        private void ConfigGetMethod<TEntity>(TEntity[] entities)
        {
            switch (entities)
            {
                case Project[]:
                    A.CallTo(() => _unitOfWork.Projects.GetAll(false)).ReturnsLazily(p => (IEnumerable<Project>)entities.ToList());
                    break;
                case Task[]:
                    A.CallTo(() => _unitOfWork.Tasks.GetAll(false)).ReturnsLazily(p => (IEnumerable<Task>)entities.ToList());
                    break;
                case Team[]:
                    A.CallTo(() => _unitOfWork.Teams.GetAll(false)).ReturnsLazily(p => (IEnumerable<Team>)entities.ToList());
                    break;
                case User[]:
                    A.CallTo(() => _unitOfWork.Users.GetAll(false)).ReturnsLazily(p => (IEnumerable<User>)entities.ToList());
                    break;
            }
        }
        [Fact]
        public void GetUserProjectsWithQuantityTask_WhenExistingUserIdWhoHasTasks_ThenGetNotEmptyDictionary()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1 } });
            ConfigGetMethod(new Task[] { new Task { Id = 1, ProjectId = 1, PerformerId = 1 },
                                         new Task { Id = 2, ProjectId = 1, PerformerId = 1 } });
 
            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var dictionary = _linqSelectionService.GetUserProjectsWithQuantityTask(1);
            // assert
            var key = dictionary.Keys.Where(p => p.Id == 1).FirstOrDefault();
            Assert.Equal(2, dictionary[key]);
        }
        [Fact]
        public void GetUserProjectsWithQuantityTask_WhenNotExistingUserId_ThenGetEmptyDictionary()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 2 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 2, TeamId = 1 } });
            ConfigGetMethod(new Task[] { new Task { Id = 1, ProjectId = 1, PerformerId = 2 },
                                         new Task { Id = 2, ProjectId = 1, PerformerId = 2 } });

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var dictionary = _linqSelectionService.GetUserProjectsWithQuantityTask(1);
            // assert
            Assert.Empty(dictionary);
        }
        [Fact]
        public void GetUserTasks_WhenExistingUserId_ThenGetTasksWithNamesLess45letters()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1 } });
            ConfigGetMethod(new Task[] { new Task { Id = 1, ProjectId = 1, PerformerId = 1, Name = new string('a', 5) },
                                         new Task { Id = 2, ProjectId = 1, PerformerId = 1, Name = new string('a', 45) } });

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var list = _linqSelectionService.GetUserTasks(1).ToArray();
            // assert
            Assert.Single(list);
            Assert.Equal(5, list[0].Name.Length);

        }
        [Fact]
        public void GetUserTasks_WhenNotExistingUserId_ThenEmptyList()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 2 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 2, TeamId = 1 } });
            ConfigGetMethod(new Task[] { new Task { Id = 1, ProjectId = 1, PerformerId = 2, Name = new string('a', 5) },
                                         new Task { Id = 2, ProjectId = 1, PerformerId = 2, Name = new string('a', 45) } });

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var list = _linqSelectionService.GetUserTasks(1);
            // assert
            Assert.Empty(list);
        }
        [Fact]
        public void GetFinishedTaskInCurrentYear_WhenExistingUserId_ThenGetTasksFinishedIn2021()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1 } });
            ConfigGetMethod(new Task[] { new Task { Id = 1, ProjectId = 1, PerformerId = 1, FinishedAt = new DateTime(2021, 1, 1) },
                                         new Task { Id = 2, ProjectId = 1, PerformerId = 1, FinishedAt = new DateTime(2020, 1, 1) },
                                         new Task { Id = 3, ProjectId = 1, PerformerId = 1, FinishedAt = null } });

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            FinishedTaskDTO[] list = _linqSelectionService.GetFinishedTaskInCurrentYear(1).ToArray();
            // assert
            Assert.Single(list);
            Assert.Equal(1, list[0].Id);

        }
        [Fact]
        public void GetFinishedTaskInCurrentYear_WhenNotExistingUserId_ThenEmptyList()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 2 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 2, TeamId = 1 } });
            ConfigGetMethod(new Task[] { new Task { Id = 1, ProjectId = 2, PerformerId = 1, FinishedAt = new DateTime(2021, 1, 1) },
                                         new Task { Id = 2, ProjectId = 2, PerformerId = 1, FinishedAt = new DateTime(2020, 1, 1) },
                                         new Task { Id = 3, ProjectId = 2, PerformerId = 1, FinishedAt = null } });
            
            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var list = _linqSelectionService.GetFinishedTaskInCurrentYear(1);
            // assert
            Assert.Empty(list);
        }
        [Fact]
        public void GetTeamShortInfo_WhenExistTeamWithMembers10yearsplus_ThenListWithTeamsWhichMembersOrderedDescByRegisteredDate()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1 },
                                         new User { Id = 2, TeamId = 1, Birthday = new DateTime(2011, 1, 1), RegisteredAt = new DateTime(2011, 1, 1) },
                                         new User { Id = 3, TeamId = 1, Birthday = new DateTime(2000, 1, 1), RegisteredAt = new DateTime(2010, 1, 1) },
                                         new User { Id = 4, TeamId = 1, Birthday = new DateTime(1999, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) },
                                         new User { Id = 5, TeamId = 2, Birthday = new DateTime(2012, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) },
                                         new User { Id = 6, TeamId = 2, Birthday = new DateTime(2000, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) }});
            ConfigGetMethod(new Team[] { new Team { Id = 1 },
                                           new Team { Id = 2 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1 },
                                            new Project { Id = 2, AuthorId = 1, TeamId = 2 }});

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            TeamShortInfoDTO[] list = _linqSelectionService.GetTeamShortInfo().ToArray();
            // assert
            Assert.Single(list);
            // check ordering by registered date
            var expectedList = list[0].Members.OrderByDescending(p => p.RegisteredAt);
            Assert.True(expectedList.SequenceEqual(list[0].Members));
        }
        [Fact]
        public void GetTeamShortInfo_WhenNotExistTeamWithMembers10yearsplus_ThenEmptyList()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1 },
                                         new User { Id = 2, TeamId = 1, Birthday = new DateTime(2011, 1, 1), RegisteredAt = new DateTime(2011, 1, 1) },
                                         new User { Id = 3, TeamId = 1, Birthday = new DateTime(2015, 1, 1), RegisteredAt = new DateTime(2010, 1, 1) },
                                         new User { Id = 4, TeamId = 1, Birthday = new DateTime(1999, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) },
                                         new User { Id = 5, TeamId = 2, Birthday = new DateTime(2012, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) },
                                         new User { Id = 6, TeamId = 2, Birthday = new DateTime(2000, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) }});
            ConfigGetMethod(new Team[] { new Team { Id = 1 },
                                         new Team { Id = 2 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1 },
                                            new Project { Id = 2, AuthorId = 1, TeamId = 2 }});

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var list = _linqSelectionService.GetTeamShortInfo();
            // assert
            Assert.Empty(list);
        }
        [Fact]
        public void GetUsersWithTasks_WhenExistUserWithTask_ThenListUsersOrderedByFirstNameWhichTasksOrderedDescByLengthName()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1, FirstName = "A" },
                                         new User { Id = 2, FirstName = "Y" },
                                         new User { Id = 3, FirstName = "K" } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1 },
                                            new Project { Id = 2, AuthorId = 1, TeamId = 1 }});
            ConfigGetMethod(new Task[] { new Task { Id = 1, PerformerId = 1, ProjectId = 1, Name = new string('a', 1) },
                                         new Task { Id = 2, PerformerId = 1, ProjectId = 2, Name = new string('a', 3) },
                                         new Task { Id = 3, PerformerId = 1, ProjectId = 1, Name = new string('a', 2) },
                                         new Task { Id = 4, PerformerId = 2, ProjectId = 2, Name = new string('a', 1) },
                                         new Task { Id = 5, PerformerId = 3, ProjectId = 1, Name = new string('a', 1) } });

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            PerformerWithTasksDTO[] list = _linqSelectionService.GetUsersWithTasks().ToArray();
            // assert
            var expectedUserList = list.OrderBy(p => p.FirstName);
            Assert.True(expectedUserList.SequenceEqual(list));
            var expectedTaskList = list[0].Tasks.OrderByDescending(p => p.Name.Length);
            Assert.True(expectedTaskList.SequenceEqual(list[0].Tasks));
        }
        [Fact]
        public void GetUsersWithTasks_WhenNotExistUserWithTasks_ThenEmptyList()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1, FirstName = "A" },
                                         new User { Id = 2, FirstName = "Y" },
                                         new User { Id = 3, FirstName = "K" } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1 },
                                            new Project { Id = 2, AuthorId = 1, TeamId = 1 }});

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var list = _linqSelectionService.GetUsersWithTasks();
            // assert
            Assert.Empty(list);
        }
        [Fact]
        public void GetUserDetail_WhenExistUserIdWithProject_ThenStructureUserDetail()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1 },
                                         new User { Id = 2 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, CreatedAt = new DateTime(2021, 1, 1), TeamId = 1 },
                                            new Project { Id = 2, AuthorId = 1, CreatedAt = new DateTime(2020, 1, 1), TeamId = 1 },
                                            new Project { Id = 3, AuthorId = 1, CreatedAt = new DateTime(2019, 1, 1), TeamId = 1 },
                                            new Project { Id = 4, AuthorId = 2, TeamId = 1 }});
            ConfigGetMethod(new Task[] { new Task { Id = 1, PerformerId = 1, ProjectId = 1, CreatedAt = new DateTime(2020, 1, 1), FinishedAt = null},
                                         new Task { Id = 2, PerformerId = 2, ProjectId = 1},
                                         new Task { Id = 3, PerformerId = 2, ProjectId = 1},
                                         new Task { Id = 4, PerformerId = 1, ProjectId = 2, CreatedAt = new DateTime(2019, 1, 1), FinishedAt = null},
                                         new Task { Id = 5, PerformerId = 1, ProjectId = 3, CreatedAt = new DateTime(2017, 1, 1), FinishedAt = new DateTime(2021, 1, 1)},
                                         new Task { Id = 6, PerformerId = 1, ProjectId = 2, CreatedAt = new DateTime(2019, 1, 1), FinishedAt = new DateTime(2021, 1, 1)},
                                         new Task { Id = 7, PerformerId = 1, ProjectId = 3, CreatedAt = new DateTime(2010, 1, 1), FinishedAt = new DateTime(2021, 1, 1)} });
            
            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            UserDetailDTO structure = _linqSelectionService.GetUserDetail(1);
            // assert
            Assert.Equal(1, structure.User.Id);
            Assert.Equal(1, structure.LastProject.Id);
            Assert.Equal(3, structure.ProjectTasksCount);
            Assert.Equal(2, structure.UserUnfinishedTasksCount);
            Assert.Equal(7, structure.LongestTask.Id);

        }
        [Fact]
        public void GetUserDetail_WhenNotExistUserId_ThenGetNull()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1 },
                                         new User { Id = 2 } });
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] {new Project { Id = 1, AuthorId = 1, CreatedAt = new DateTime(2021, 1, 1), TeamId = 1 },
                                           new Project { Id = 2, AuthorId = 1, CreatedAt = new DateTime(2020, 1, 1), TeamId = 1 },
                                           new Project { Id = 3, AuthorId = 1, CreatedAt = new DateTime(2019, 1, 1), TeamId = 1 },
                                           new Project { Id = 4, AuthorId = 2, TeamId = 1 }});
            ConfigGetMethod(new Task[] { new Task { Id = 1, PerformerId = 1, ProjectId = 1, CreatedAt = new DateTime(2020, 1, 1), FinishedAt = null},
                                         new Task { Id = 2, PerformerId = 2, ProjectId = 1},
                                         new Task { Id = 3, PerformerId = 2, ProjectId = 1},
                                         new Task { Id = 4, PerformerId = 1, ProjectId = 2, CreatedAt = new DateTime(2019, 1, 1), FinishedAt = null},
                                         new Task { Id = 5, PerformerId = 1, ProjectId = 3, CreatedAt = new DateTime(2017, 1, 1), FinishedAt = new DateTime(2021, 1, 1)},
                                         new Task { Id = 6, PerformerId = 1, ProjectId = 2, CreatedAt = new DateTime(2019, 1, 1), FinishedAt = new DateTime(2021, 1, 1)},
                                         new Task { Id = 7, PerformerId = 1, ProjectId = 3, CreatedAt = new DateTime(2010, 1, 1), FinishedAt = new DateTime(2021, 1, 1)} });

            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            UserDetailDTO structure = _linqSelectionService.GetUserDetail(0);
            // assert
            Assert.Null(structure);
        }
        [Fact]
        public void GetProjectsDetail_WhenExistProject_ThenListProjectDetail()
        {
            // arrange 
            ConfigGetMethod(new User[] { new User { Id = 1, TeamId = 1 },
                                         new User { Id = 2, TeamId = 1 }});
            ConfigGetMethod(new Team[] { new Team { Id = 1 } });
            ConfigGetMethod(new Project[] { new Project { Id = 1, AuthorId = 1, TeamId = 1, Description = new string('a', 21) } });
            ConfigGetMethod(new Task[] { new Task { Id = 1, PerformerId = 1, ProjectId = 1, Name = new string('a', 1), Description = new string('a', 2)},
                                         new Task { Id = 2, PerformerId = 2, ProjectId = 1, Name = new string('a', 2), Description = new string('a', 1)} });
            
            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var list = _linqSelectionService.GetProjectsDetail().ToList();
            // assert
            Assert.Equal(1, list[0].Project.Id);
            Assert.Equal(1, list[0].LongestDescriptionTask.Id);
            Assert.Equal(1, list[0].ShortestNameTask.Id);
            Assert.Equal(2, list[0].TeamMembersCount);
        }
        [Fact]
        public void GetProjectsDetail_WhenNotExistProject_ThenEmptyList()
        {
            // arrange 
            ILinqSelectionService _linqSelectionService = new LinqSelectionService(_unitOfWork, _mapper);
            // act
            var list = _linqSelectionService.GetProjectsDetail();
            // assert
            Assert.Empty(list);
        }
    }
}
