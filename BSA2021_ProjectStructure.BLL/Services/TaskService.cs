﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<TaskDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_unitOfWork.Tasks.GetAll(false));
        }
        public TaskDTO FindById(int taskId)
        {
            return _mapper.Map<TaskDTO>(_unitOfWork.Tasks.FindById(taskId));
        }
        public TaskDTO Insert(NewTaskDTO taskDTO)
        {
            var createdTask = _mapper.Map<Task>(taskDTO);
            _unitOfWork.Tasks.Insert(createdTask);
            _unitOfWork.SaveChanges();
            return _mapper.Map<TaskDTO>(createdTask);
        }

        public void Update(TaskDTO taskDTO)
        {
            Task editTask = _unitOfWork.Tasks.FindById(taskDTO.Id);
            _mapper.Map<TaskDTO, Task>(taskDTO, editTask);
            _unitOfWork.Tasks.Update(editTask);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int taskId)
        {
            _unitOfWork.Tasks.Delete(taskId);
            _unitOfWork.SaveChanges();
        }

        public void Delete(TaskDTO taskDTO)
        {
            _unitOfWork.Tasks.Delete(_mapper.Map<Task>(taskDTO));
            _unitOfWork.SaveChanges();
        }
        public bool CheckAvailability(int taskId)
        {
            return _unitOfWork.Tasks.CheckAvailability(taskId);
        }
       
    }
}
