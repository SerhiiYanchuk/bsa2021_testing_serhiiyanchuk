﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAll();
        UserDTO FindById(int id);
        UserDTO Insert(NewUserDTO item);
        void Update(UserDTO item);
        void Delete(int id);
        void Delete(UserDTO item);
        bool CheckAvailability(int id);
        IEnumerable<TaskDTO> FindUserUnfinishedTasks(int id);
    }
}
