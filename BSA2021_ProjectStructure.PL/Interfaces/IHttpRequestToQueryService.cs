﻿using BSA2021_ProjectStructure.PL.Models;
using BSA2021_ProjectStructure.PL.Models.QueryResultModels;
using System;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.PL.Interfaces
{
    public interface IHttpRequestToQueryService: IDisposable
    {
        IDictionary<ProjectModel, int> GetUserProjectsWithQuantityTask(int performerId);
        IEnumerable<TaskModel> GetUserTasks(int performerId);
        IEnumerable<FinishedTaskModel> GetFinishedTaskInCurrentYear(int performerId);
        IEnumerable<TeamShortInfoModel> GetTeamShortInfo();
        IEnumerable<PerformerWithTasksModel> GetUsersWihtTasks();
        UserDetailModel GetUserDetail(int userId);
        IEnumerable<ProjectDetailModel> GetProjectsDetail();
    }
}
