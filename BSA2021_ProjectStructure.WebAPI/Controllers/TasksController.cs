﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA2021_TaskStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetTasks()
        {
            return Ok(_taskService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetTask(int id)
        {
            var task = _taskService.FindById(id);
            if (task is null)
                return NotFound("ID doesn't exist");
            return Ok(task);
        }

        [HttpPost]
        public ActionResult<TaskDTO> PostTask([FromBody] NewTaskDTO taskDTO)
        {
            var createdTask = _taskService.Insert(taskDTO);
            return Created("/api/tasks/" + createdTask.Id, createdTask);
        }

        [HttpPut]
        public IActionResult PutTask([FromBody] TaskDTO taskDTO)
        {
            if (!_taskService.CheckAvailability(taskDTO.Id))
                return NotFound("ID doesn't exist");

            _taskService.Update(taskDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTask(int id)
        {
            if (!_taskService.CheckAvailability(id))
                return NotFound("ID doesn't exist");

            _taskService.Delete(id);
            return NoContent();
        }
    }
}
