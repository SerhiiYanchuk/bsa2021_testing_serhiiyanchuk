﻿using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface ITeamRepository : IRepository<Team>
    {
        // you can add specific functionality for Team
        void LoadMembers(Team team);
        void LoadProjects(Team team);
    }
}
