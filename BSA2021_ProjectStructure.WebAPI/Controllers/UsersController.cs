﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace BSA2021_UserStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILinqSelectionService _linqSelectionService;

        public UsersController(IUserService userService, ILinqSelectionService linqSelectionService)
        {
            _userService = userService;
            _linqSelectionService = linqSelectionService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetUsers()
        {
            return Ok(_userService.GetAll());
        }
        [HttpGet("tasks")]
        public ActionResult<IEnumerable<PerformerWithTasksDTO>> GetUsersWithTasks()
        {
            return Ok(_linqSelectionService.GetUsersWithTasks());
        }
        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetUser(int id)
        {
            var user = _userService.FindById(id);
            if (user is null)
                return NotFound("ID doesn't exist");
            return Ok(user);
        }
        [HttpGet("{id}/projects")]
        public ActionResult<IDictionary<ProjectDTO, int>> GetUserProjectsWithQuantityTask(int id)
        {
            if (!_userService.CheckAvailability(id))
                return NotFound("ID doesn't exist");
            return Ok(_linqSelectionService.GetUserProjectsWithQuantityTask(id).ToArray());
        }
        [HttpGet("{id}/tasks")]
        public ActionResult<IEnumerable<TaskDTO>> GetUserTasks(int id)
        {
            if (!_userService.CheckAvailability(id))
                return NotFound("ID doesn't exist");
            return Ok(_linqSelectionService.GetUserTasks(id));
        }
        [HttpGet("{id}/tasks/finished")]
        public ActionResult<IEnumerable<FinishedTaskDTO>> GetFinishedTaskInCurrentYear(int id)
        {
            if (!_userService.CheckAvailability(id))
                return NotFound("ID doesn't exist");
            return Ok(_linqSelectionService.GetFinishedTaskInCurrentYear(id));
        }
        [HttpGet("{id}/tasks/unfinished")]
        public ActionResult<IEnumerable<TaskDTO>> GetUserUnfinishedTasks(int id)
        {
            if (!_userService.CheckAvailability(id))
                return NotFound("ID doesn't exist");
            return Ok(_userService.FindUserUnfinishedTasks(id));
        }
        [HttpGet("{id}/detail")]
        public ActionResult<UserDetailDTO> GetUserDetail(int id)
        {
            if (!_userService.CheckAvailability(id))
                return NotFound("ID doesn't exist");
            return Ok(_linqSelectionService.GetUserDetail(id));
        }
        [HttpPost]
        public ActionResult<UserDTO> PostUser([FromBody] NewUserDTO userDTO)
        {
            var createdUser = _userService.Insert(userDTO);
            return Created("/api/users/" + createdUser.Id, createdUser);
        }

        [HttpPut]
        public IActionResult PutUser([FromBody] UserDTO userDTO)
        {
            if (!_userService.CheckAvailability(userDTO.Id))
                return NotFound("ID doesn't exist");

            _userService.Update(userDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            if (!_userService.CheckAvailability(id))
                return NotFound("ID doesn't exist");

            _userService.Delete(id);
            return NoContent();
        }
    }
}
