﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetAll();
        TeamDTO FindById(int id);
        TeamDTO Insert(NewTeamDTO item);
        void Update(TeamDTO item);
        void Delete(int id);
        void Delete(TeamDTO item);
        bool CheckAvailability(int id);
    }
}
