﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class NewTeamModel
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
