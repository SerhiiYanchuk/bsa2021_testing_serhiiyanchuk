using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.WebAPI;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace BSA2021_ProjectStructure.IntegrationTests
{
    public class TeamControllerIntegrationTests: IClassFixture<CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup>>
    {
        private readonly HttpClient _client;
        public TeamControllerIntegrationTests(CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup> factory)
        {
            _client = factory.CreateClient();
        }
        [Fact]
        public async System.Threading.Tasks.Task PostTeam_WhenValidBody_ThenResponseCode201AndGetByIdResponseCode200()
        {
            // arrange
            string jsonInString = JsonConvert.SerializeObject(new NewTeamDTO());

            // act/assert
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResponce = await httpResponse.Content.ReadAsStringAsync();
            int generatedId = JsonConvert.DeserializeObject<TeamDTO>(stringResponce).Id;

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/teams/{generatedId}");
            Assert.Equal(HttpStatusCode.OK, httpResponseGetById.StatusCode);

            await _client.DeleteAsync($"api/teams/{generatedId}");
        }
        [Theory]
        [InlineData("some data")]
        public async System.Threading.Tasks.Task PostTeam_WhenNotValidBody_ThenResponseCode400(string jsonInString)
        {
            // act
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            // assert
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
