﻿using System;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime Birthday { get; set; }
        public override string ToString()
        {
            return $"ID: {Id}, Name: {FirstName} {LastName}, Birthday: {Birthday}";
        }
    }
}


