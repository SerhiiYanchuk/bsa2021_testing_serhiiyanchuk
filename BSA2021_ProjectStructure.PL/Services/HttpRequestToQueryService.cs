﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using BSA2021_ProjectStructure.PL.Models.QueryResultModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BSA2021_ProjectStructure.PL.Services
{
    public class HttpRequestToQueryService: IHttpRequestToQueryService
    {
        private readonly string _serverURL;
        private readonly HttpClient _client = new HttpClient();
        public HttpRequestToQueryService(string serverURL)
        {
            _serverURL = serverURL ?? "https://localhost:44361";
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        private HttpResponseMessage GetData(string route)
        {
            var response = _client.GetAsync(_serverURL + route).Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        public IDictionary<ProjectModel, int> GetUserProjectsWithQuantityTask(int performerId)
        {
            var response = GetData($"/api/users/{performerId}/projects");
            if (response is not null)
            {
                string serializedArray = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<ProjectModel, int>>>(serializedArray).ToDictionary(p => p.Key, p => p.Value);
            }
            return new Dictionary<ProjectModel, int>();
        }

        public IEnumerable<TaskModel> GetUserTasks(int performerId)
        {
            var response = GetData($"/api/users/{performerId}/tasks");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<TaskModel>>(response.Content.ReadAsStringAsync().Result);
            return new List<TaskModel>();       
        }

        public IEnumerable<FinishedTaskModel> GetFinishedTaskInCurrentYear(int performerId)
        {
            var response = GetData($"/api/users/{performerId}/tasks/finished");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<FinishedTaskModel>>(response.Content.ReadAsStringAsync().Result);
            return new List<FinishedTaskModel>();        
        }

        public IEnumerable<TeamShortInfoModel> GetTeamShortInfo()
        {
            var response = GetData($"/api/teams/info");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<TeamShortInfoModel>>(response.Content.ReadAsStringAsync().Result);
            return new List<TeamShortInfoModel>();
        }

        public IEnumerable<PerformerWithTasksModel> GetUsersWihtTasks()
        {
            var response = GetData($"/api/users/tasks");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<PerformerWithTasksModel>>(response.Content.ReadAsStringAsync().Result);
            return new List<PerformerWithTasksModel>();
        }

        public UserDetailModel GetUserDetail(int userId)
        {
            var response = GetData($"/api/users/{userId}/detail");
            if (response is not null)
                return JsonConvert.DeserializeObject<UserDetailModel>(response.Content.ReadAsStringAsync().Result);
            return null;
        }

        public IEnumerable<ProjectDetailModel> GetProjectsDetail()
        {
            var response = GetData($"/api/projects/detail");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<ProjectDetailModel>>(response.Content.ReadAsStringAsync().Result);
            return new List<ProjectDetailModel>();
        }
      
        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}
